import paho.mqtt.client as mqttClient
import time
import json

class MqttHelper:

  def __init__(self,clientId,host,port,username,password,subTopicLWT,subTopicRequest,pubTopicCS):

    self.host = host
    self.port = port
    self.username = username
    self.password = password
    self.clientId = clientId
    self.subTopicLWT = subTopicLWT
    self.subTopicRequest = subTopicRequest
    self.pubTopicCS = pubTopicCS
    self.client_list = []
    self.lwm = ""
    self.lwm_user = ""
    self.mqttClient = mqttClient.Client(self.clientId)
    self.mqttClient.username_pw_set(username, password=password)
    self.mqttClient.on_connect = self.on_connect
    self.mqttClient.on_message = self.on_message
    self.mqttClient.connect(host, port=int(port))
    self.mqttClient.loop_start()

  def on_connect(self, client, userdata, flags, rc):
    print("Connected to broker !")
    self.mqttClient.subscribe(self.subTopicRequest)
    self.mqttClient.subscribe(self.subTopicLWT)
    print("Subscribed to topic: "+self.subTopicRequest+" , waiting for chatbook users to send connection status...")
    print("Subscribed to topic: "+self.subTopicRequest+" , waiting for LWT messages...")

  def on_message(self,client, userdata, message):

    # When User Will Log Off The Chatbook Account
    if message.topic == self.subTopicLWT:
      self.lwm = message.payload
      print("Last Will Message Received : " + message.payload)
      self.lwm_user = self.lwm.split()[1]
      # Searching Client List And Setting Its Status Either 0 or 1
      if(int(len(self.client_list))):
        temp_dict={}
        search_found = False
        for dictionary in self.client_list:
          if dictionary["username"] == self.lwm_user:
            search_found = True
            dictionary["status"] = 0
            print("Client Already Registered And Status Got Updated")
            print("Client List : ")
            print(self.client_list)
            break

        if not(search_found):
          temp_dict["username"] = self.username
          temp_dict["status"] = 0
          self.client_list.append(temp_dict.copy())
          print("New Client Registered And Status Got Updated")
          print("Client List : ")
          print(self.client_list)

      print("Sending Client List...")
      self.mqttClient.publish(self.pubTopicCS, json.dumps(self.client_list))
      print("Client List Sended Successfully !")

    # When User Will Log In To Chatbook Account
    if message.topic == self.subTopicRequest:
      print("Message Received For Registration : "+message.payload)
      temp_dict={}
      search_found = False
      for dictionary in self.client_list:
        if dictionary["username"] == message.payload:
          search_found = True
          dictionary["status"] = 1
          print("Client Already Registered And Status Got Updated")
          print(self.client_list)
          break

      if not(search_found):
        temp_dict["username"] = message.payload
        temp_dict["status"] = 1
        self.client_list.append(temp_dict.copy())
        print("New Client Registered And Status Got Updated")
        print("Client List : ")
        print(self.client_list)

      print("Sending Client List...")
      self.mqttClient.publish(self.pubTopicCS, json.dumps(self.client_list))
      print("Client List Sended Successfully !")



  def disconnect(self):    
    self.mqttClient.disconnect()

obj = MqttHelper(clientId = "chatbookhandler", host = "127.0.0.1", port = "1883", username = "chatbook", password = "chatbook", subTopicLWT = "chatbook/lastwill", subTopicRequest = "chatbook/updateclientlist", pubTopicCS = "chatbook/returnclientlist")

while True:
  time.sleep(0.1)